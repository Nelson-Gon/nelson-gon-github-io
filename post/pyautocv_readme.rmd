---
date: "`r Sys.Date()`"
title: "Automated Image Processing with pyautocv in Python"
output: github_document
type: "post"
categories: ["python","image analysis", "image processing", "computer vision"]
tags: ["python","image processing","computer vision","image analysis"]
---

`pyautocv`: **(Semi) Automated Image Processing**




**Project Aims**

The goal of pyautocv is to provide a simple computer vision(cv) workflow that enables one to automate 
or at least reduce the time spent in image (pre)-processing. 


**Installing the package**

From pypi:


```{r, eval=FALSE}

pip install pyautocv

```

From GitHub

```{r, eval=FALSE}

pip install pip install git+https://github.com/Nelson-Gon/pyautocv.git
#or
# clone the repo
git clone https://www.github.com/Nelson-Gon/pyautocv.git
cd pyautocv
python3 setup.py install


```

**Available Classes**


* Segmentation is a super class on which other classes build

* EdgeDetection is dedicated to edge detection. Currently supported kernels are stored in `.available_operators()`

* Thresholding dedicated to thresholding.



**Example Usage**

* Smoothing

To smooth a directory of images, we can use `EdgeDetection`'s `smooth` method as
follows:

``````{r, eval=FALSE}

to_smooth = EdgeDetection("images/people","sobel_vertical","box")
show_images(*[to_smooth.gray_images(), to_smooth.smooth()])

```

This will give us;

![Smoothened](sample_results/smooth_images.png)


* Edge Detection 

To detect edges in a directory image, we provide original(grayed) images for comparison to
images that have been transformed to detect edges. 

```{r, eval=FALSE}

from pyautocv.segmentation import *
edge_detection = EdgeDetection("images","sobel_vertical","box")

show_images(*[edge_detection.gray_images(), edge_detection.detect_edges()])


```

The above will give us the following result:


![Sample_colored](./sample_results/images_smooth.png)

To use a different filter e.g Laplace,

```{r, eval=FALSE}

show_images(*[edge_detection.gray_images(), edge_detection.detect_edges(operator="laplace")])

```

This results in:

![Laplace](./sample_results/show_smooth_laplace.png)


* Thresholding

To perform thresholding, we can use `Threshold`'s methods dedicated to thresholding.

We use flowers as an example:

```{r, eval=FALSE}

to_threshold = Threshold("images/flowers",threshold_method="simple")
show_images(to_threshold.gray_images(),to_threshold.threshold_images())


```

![Flowers](./sample_results/threshold_flowers.png)



These and more examples are available in [example2.py](./examples/example2.py). Image sources are
shown in `sources.md`. If you feel, attribution was not made, please file an issue
and cite the violating image.

> Thank you very much

> “A language that doesn't affect the way you think about programming is not worth knowing.”
― Alan J. Perlis


---

References:

* [Bebis](https://www.cse.unr.edu/~bebis/CS791E/Notes/EdgeDetection.pdf)
